//
//  TypeOfUser.swift
//  British Hills
//
//  Created by Kadir Kadyrov on 25.05.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import Foundation

enum TypeOfUser: String {
    case teacher = "Teacher"
    case admin = "Admin"
    case student = "Student"
}
