//
//  LessonsVC.swift
//  British Hills
//
//  Created by Kadir Kadyrov on 28.05.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

class LessonsVC: UIViewController {
    
    @IBOutlet weak var lessonsTable: UITableView!
    var dateOfLessons: [String] = ["27.09", "30.09", "6.05", "04.04"]

    override func viewDidLoad() {
        super.viewDidLoad()
        lessonsTable.delegate = self
        lessonsTable.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    


}

extension LessonsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dateOfLessons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = lessonsTable.dequeueReusableCell(withIdentifier: "LessonsCell") as? LessonCell {
            cell.update(date: dateOfLessons[indexPath.row])
            return cell
        } else {
            return LessonCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "LessonsTabBar", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let segue = segue.destination as? LessonTabBar {
            let index = sender as! IndexPath
            segue.initVC(date: dateOfLessons[index.row])
        }
    }
}
