//
//  MainVC.swift
//  British Hills
//
//  Created by Kadir Kadyrov on 25.05.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    @IBOutlet weak var loginText: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var passwordText: UITextField!
    
    var user:TypeOfUser = .teacher

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        hiddeNavigation()
        setButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        hiddeNavigation()
    }
    
    @IBAction func onTapSignUp(_ sender: Any) {
        showNavigation()
        performSegue(withIdentifier: "SignUpVC", sender: nil)
    }
    
    @IBAction func onTapLogin(_ sender: Any) {
        showNavigation()
        login()
        
    }
    
    func login () {
        
        switch user {
        case .student:
            performSegue(withIdentifier: "Student", sender: nil)
        case .admin:
            performSegue(withIdentifier: "Admin", sender: nil)
        case .teacher:
            performSegue(withIdentifier: "Teacher", sender: nil)
        }
    }
    
    func hiddeNavigation () {
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func showNavigation () {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func setButton () {
        loginButton.layer.cornerRadius =  loginButton.frame.height / 2
        signUpButton.layer.cornerRadius =  signUpButton.frame.height / 2
    }

}
