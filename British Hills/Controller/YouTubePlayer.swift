//
//  YouTubePlayer.swift
//  British Hills
//
//  Created by Kadir Kadyrov on 28.05.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit
import WebKit
class YouTubePlayer: UIViewController, WKUIDelegate {
    private var url: String = ""
    
    @IBOutlet weak var web: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myURL = URL(string: self.url)
        let myRequest = URLRequest(url: myURL!)
        web.uiDelegate = self
        web.load(myRequest)

        // Do any additional setup after loading the view.
    }
    
    func initVC (wikiURL url: String) {
        self.url = url
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
