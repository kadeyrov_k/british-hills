//
//  LessonTabBar.swift
//  British Hills
//
//  Created by Kadir Kadyrov on 28.05.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

class LessonTabBar: UITabBarController {
    var date: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = date
        
        let lessonVideoVC = self.viewControllers![0] as! LessonVideoVC
        lessonVideoVC.initVC(date: date)
        let lessonHomeWorkVC = self.viewControllers![1] as! LessonHomeWorkVC
        lessonHomeWorkVC.initVC(date: date)
        
        // Do any additional setup after loading the view.
    }
    
    func initVC (date: String) {
        self.date = date
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
