//
//  LessonVideoVC.swift
//  British Hills
//
//  Created by Kadir Kadyrov on 28.05.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

class LessonVideoVC: UIViewController {
    @IBOutlet weak var videoTable: UITableView!
    
    var date: String = ""
    
    var urlOfVideos: [String] = ["https://www.youtube.com/watch?v=Erh7Ot8bs94", "https://www.youtube.com/watch?v=zPFHKGDWx84"]

    override func viewDidLoad() {
        super.viewDidLoad()
        videoTable.delegate = self
        videoTable.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    func initVC (date: String) {
        self.date = date
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LessonVideoVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        urlOfVideos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = videoTable.dequeueReusableCell(withIdentifier: "VideoCell") as? VideoCell {
            cell.update(nameOfVideo: urlOfVideos[indexPath.row])
            return cell
        }
        else {
            return VideoCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "YouTubePlayer", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let index = sender as! IndexPath
        
        if let VC = segue.destination as? YouTubePlayer {
            VC.initVC(wikiURL: urlOfVideos[index.row])
        }
    }
    
}
