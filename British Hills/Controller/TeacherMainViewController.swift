//
//  TeacherMainViewController.swift
//  British Hills
//
//  Created by Kadir Kadyrov on 23.07.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import UIKit

class TeacherMainViewController: UIViewController {
    @IBOutlet weak var groupsTableView: UITableView!
    
    private var groups: [String: [String]] =  [
        "Elementary": ["Group 1", "Group 2"],
        "Pre-intermidiate": ["Group 3", "Group 8", "Group 11"],
        "Intermidiate": ["Group 7", "Group 9"]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        groupsTableView.delegate = self
        groupsTableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
}

extension TeacherMainViewController: UITableViewDelegate {
    
}

extension TeacherMainViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        groups.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        <#code#>
    }
    
    
}
