//
//  User.swift
//  British Hills
//
//  Created by Kadir Kadyrov on 23.07.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import Foundation

protocol User {
    var name: String {get set}
    var surname: String {get set}
    var password: passwd {get set}
    var login: String {get set}
    
    mutating func changePassword(newPassword: passwd)
    mutating func changeName(newName: String)
    mutating func changeSurname(newSurname: String)
    mutating func changeLogin(newLogin: String)
}
