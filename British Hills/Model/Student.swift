//
//  Student.swift
//  British Hills
//
//  Created by Kadir Kadyrov on 23.07.2020.
//  Copyright © 2020 Kadir Kadyrov. All rights reserved.
//

import Foundation

struct Student: User {
    var name: String
    
    var surname: String
    
    var password: passwd
    
    var login: String
    
    var groupName: String
    
    mutating func changePassword(newPassword: passwd) {
        password = newPassword
    }
    
    mutating func changeName(newName: String) {
        name = newName
    }
    
    mutating func changeSurname(newSurname: String) {
        surname = newSurname
    }
    
    mutating func changeLogin(newLogin: String) {
        login = newLogin
    }
    
    mutating func changeGroupName(newGroupName: String) {
        groupName = newGroupName
    }
    
}
